﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

[RequireComponent(typeof(Rigidbody2D))]
public class ChracterMoveController : MonoBehaviour
{
    [SerializeField] private CharacterAnimationController _characterAnimationController;
    [SerializeField] private float _moveForce = 10;
    [SerializeField] private float _maxSpeedAnimationMultiplier = 3;
    
    private Rigidbody2D _body;
    
    private void Awake()
    {
        _body = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        var horizontalAxis = Input.GetAxis("Horizontal");

        if (horizontalAxis == 0) 
            _body.velocity = new Vector2(0, _body.velocity.y);
        else
            _body.AddForce(new Vector2(_moveForce * horizontalAxis, 0));

        var velocityXAbs = Mathf.Abs(_body.velocity.x);
        
        var percentageVelocityOfMaxSpeed =
            1.0f - (_maxSpeedAnimationMultiplier - velocityXAbs) / _maxSpeedAnimationMultiplier;
        
        _characterAnimationController.Animator.SetFloat(_characterAnimationController.SpeedMultiplierParam, 
            1.0f + percentageVelocityOfMaxSpeed);
    }
}
