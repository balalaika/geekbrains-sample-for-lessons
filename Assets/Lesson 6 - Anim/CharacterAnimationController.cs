﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterAnimationController : MonoBehaviour
{
    [Header("Animator params")]
    [SerializeField] private string _runParamName = "IsRun";
    [SerializeField] private string _attackTriggerName = "AttackTrigger";
    [SerializeField] private string _speedMultiplierParam = "SpeedMultiplier";

    public string SpeedMultiplierParam
    {
        get => _speedMultiplierParam;
    }

    private Animator _animator;
    public Animator Animator => _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        var horizontalAxis = Input.GetAxis("Horizontal");

        if (horizontalAxis != 0)
        {
            _animator.SetBool(_runParamName, true);
        } 
        else 
            _animator.SetBool(_runParamName, false);

        if (Input.GetButtonDown("Fire1"))
        {
            _animator.ResetTrigger(_attackTriggerName);
            _animator.SetTrigger(_attackTriggerName);
        }
    }
}
