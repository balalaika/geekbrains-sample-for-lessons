﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineBomb : MonoBehaviour
{
    [SerializeField] private string _detectTag;
    [SerializeField] private float _delayBeforeDestroy;

    [SerializeField] private float _bombForce = 5000;
    [SerializeField] private float _bombRadius = 10;

    [SerializeField] private Vector3 _intend;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(_detectTag)) return;

        var otherBody = other.gameObject.GetComponent<Rigidbody>();

        if (otherBody)
        {
            otherBody.AddExplosionForce(_bombForce, transform.position + _intend, _bombRadius);
        }
        
        Destroy(other.gameObject, _delayBeforeDestroy);
        
        Destroy(gameObject);
    }
}
