﻿using System;
using System.Collections;
using Match3Prototype;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(ObjectsPool))]
public class Gun : MonoBehaviour
{
    [Header("Settings")] 
    [SerializeField] private float _shotForce = 100f;
    [SerializeField] private float _explosionRadius = 2f;
    [SerializeField] private float _timeoutBetweenShot = 0.5f;
    [SerializeField] private float _distance = 100;
    
    [Header("References")]
    [SerializeField] private GameObject _decal;
    
    [Header("Events")]
    [SerializeField] private UnityEvent _onShot;

    private ObjectsPool _decalsPool;

    private Vector3 _inputVector;

    private float _realDistance;

    private bool _isShoting = false;

    private void Awake()
    {
        _decalsPool = GetComponent<ObjectsPool>();
        _decalsPool.AddToPool(10, _decal, Vector3.zero, Quaternion.identity, null);
    }

    private void Start()
    {
        PlayerInput.OnInput += RememberInput;
    }

    private void RememberInput(Vector3 inputMoveDirection)
    {
        inputMoveDirection.Normalize();
        
        if (inputMoveDirection == Vector3.zero) return;
        
        _inputVector = inputMoveDirection;
    }

    private void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            if (!_isShoting)
            {
                _isShoting = true;
                StartCoroutine(nameof(OnAutoShot));
            }
        }
        else
            _isShoting = false;
    }

    private IEnumerator OnAutoShot()
    {
        while (_isShoting)
        {
            Action<RaycastHit> onShot =
                (hit) =>
                {
                    _realDistance = hit.distance;

                    var newDecal = _decalsPool.Get();
                    newDecal.transform.position = hit.point + hit.normal * 0.01f;
                    newDecal.transform.rotation = Quaternion.LookRotation(-hit.normal);

                    var otherBody = hit.transform.gameObject.GetComponent<Rigidbody>();
                    if (otherBody != null)
                    {
                        otherBody.AddExplosionForce(_shotForce * otherBody.mass, hit.point, _explosionRadius);
                    }
                };

            RaycastHit rayCastHit;
            if (Physics.Raycast(transform.position, _inputVector, out rayCastHit, _distance))
                onShot.Invoke(rayCastHit);

            _onShot?.Invoke();
            
            Debug.DrawRay(transform.position, _inputVector * _realDistance, Color.red);
            
            yield return new WaitForSeconds(_timeoutBetweenShot);
        }
    }
}
