﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace TutorialProject.Scripts
{
    public class BallManager : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private int _targetBallInScene;

        [SerializeField] private float _delayForCheckInSec = 1.0f;

        [Header("Events")]
        [SerializeField] private UnityEvent _onSpawnEvent;

        private void Start()
        {
            StartCoroutine(nameof(CheckBallsCount));
        }
        
        /// <summary>
        /// Контролирует численность мячей и восстанавливает её, если она меньше заданного.
        /// </summary>
        /// <returns></returns>
        private IEnumerator CheckBallsCount()
        {
            while (true)
            {
                if (Ball.CountOfBalls < _targetBallInScene)
                {
                    _onSpawnEvent?.Invoke();
                }
                
                yield return new WaitForSeconds(_delayForCheckInSec);
            }
        }
    }
}