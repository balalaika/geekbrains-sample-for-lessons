﻿using System;
using UnityEngine;

namespace TutorialProject.Scripts
{
    public class Health : MonoBehaviour
    {
        [SerializeField] private GameObject _owner;
        [SerializeField] private int _hpCount = 3;

        private void OnCollisionEnter(Collision other)
        {
            _hpCount--;

            if (_hpCount == 0)
            {
                Destroy(_owner);
            }
        }
    }
}