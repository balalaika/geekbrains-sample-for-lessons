﻿using UnityEngine;

namespace TutorialProject.Scripts
{
    [CreateAssetMenu(fileName = "Main Game Params", order = 0)]
    public class GameParams : ScriptableObject
    {
        public int CharacterSpeed;
        public int CharacteeHp;
    }
}