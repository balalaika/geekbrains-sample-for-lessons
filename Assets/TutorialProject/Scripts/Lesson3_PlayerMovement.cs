﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Lesson3_PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _moveSpeedMultiplier = 5f;

    [SerializeField, Range(0.0f, 1.0f)] private float _percentageOfSlowdown = 0.01f;
    
    public float PercentageOfSlowDown => (1 - _percentageOfSlowdown);
    
    private Rigidbody _body;

    private Vector3 _moveDirection;

    private bool _isMoved = false;
    
    void Start()
    {
        _body = GetComponent<Rigidbody>();

        PlayerInput.OnInput += Move;
    }

    private void Move(Vector3 input)
    {
        _moveDirection.Set(input.x, 0, input.z);

        // Двигаем.
        _body.AddForce(_moveDirection * _moveSpeedMultiplier);

        _isMoved = true;
    }
    
    private void Update()
    {
        // Смотрим по направлению движения.
        transform.LookAt(_moveDirection + transform.position);
    }

    private void FixedUpdate()
    {
        if (_isMoved) return;
        
        // _body.velocity = new Vector3(_body.velocity.x * PercentageOfSlowDown,
        //     _body.velocity.y,
        //     _body.velocity.z * PercentageOfSlowDown);
    }

    private void LateUpdate()
    {
        if (_moveDirection == Vector3.zero)
        {
            _isMoved = false;
        }
    }
}
