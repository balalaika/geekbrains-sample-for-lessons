﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Ball : MonoBehaviour
{
    private Rigidbody _body;

    public static int CountOfBalls = 0;

    public static UnityEvent OnBallDown;
    
    private void Awake()
    {
        _body = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        CountOfBalls++;
    }

    private void OnDestroy()
    {
        CountOfBalls--;
    }
}
