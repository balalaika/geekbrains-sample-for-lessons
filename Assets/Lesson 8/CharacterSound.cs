﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSound : MonoBehaviour
{
    [SerializeField] private AudioSource _footStep;

    public void PlayFootStep()
    {
        _footStep.Play();
    }
}
