﻿using System;
using System.Collections;
using System.Collections.Generic;
using TutorialProject.Scripts;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    [SerializeField] private GameParams _gameParams;
    
    [SerializeField] private string _triggerNameForOpen;
    [SerializeField] private Animator _doorAnimator;

    private void OnTriggerEnter(Collider other)
    {
        _doorAnimator.ResetTrigger(_triggerNameForOpen);
        _doorAnimator.SetTrigger(_triggerNameForOpen);
    }
}
